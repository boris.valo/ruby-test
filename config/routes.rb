Rails.application.routes.draw do
  get 'welcome/index'

	resources :articles do
		resources :comments
		resources :ratings
	end

  root 'welcome#index'
end
