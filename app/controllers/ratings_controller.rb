class RatingsController < ApplicationController
	def index
		@article = Article.find(params[:article_id])
		@ratings = @article.ratings.group(:value).count
	end

	def create
		@article = Article.find(params[:article_id])

		ratingValue = 0
		chosenRating = params[:rating_value]

		case chosenRating
		when "Nuda"
		   ratingValue = 1
		when "To se mi líbí"
			ratingValue = 2
		when "Málo detailů"
			ratingValue = 3
		when "Příliš mnoho detailů"
			ratingValue = 4
		end

		@rating = @article.ratings.create(value: ratingValue)
		redirect_to article_path(@article)
	end
end
